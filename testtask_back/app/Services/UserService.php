<?php
declare(strict_types=1);

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;


class UserService
{
    /**
     * @var UserRepository
     */
    private $user;

    /**
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    /**
     * @return Collection|null
     */
    public function index(): ?Collection
    {
        return $this->user->allData();
    }

    /**
     * @param $id
     * @return Model
     */
    public function find($id): ?Model
    {
        return $this->user->find($id);
    }

    /**
     * @param $params
     * @param $id
     * @return void
     */
    public function update($params, $id): void
    {
        $attributes = $params->all();

        $this->user->update($attributes, $id);
    }

    /**
     * @param $params
     */
    public function create($params): void
    {
        $attributes = $params->all();

        $this->user->create($attributes);
    }

    /**
     * @param int $id
     */
    public function delete(int $id)
    {
        $this->user->delete($id);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getUserPayments(int $id)
    {
        return $this->user->find($id)->payments;
    }
}
