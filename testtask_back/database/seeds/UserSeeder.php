<?php
declare(strict_types=1);

use App\Models\Payment;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Amount of created users
     */
    const USER_AMOUNT = 5;

    /**
     * Amount of created payments
     */
    const PAYMENT_AMOUNT = 3;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, self::USER_AMOUNT)->create()->each(function ($user) {
            $user->payments()->saveMany(factory(Payment::class, self::PAYMENT_AMOUNT)->make());
        });
    }
}
