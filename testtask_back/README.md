# laravel vue test task

## Project setup
```
composer install
```
### Copy .env.example file as .env
```
php artisan key:generate
```
### Create database
```
php artisan migrate
```
```
php artisan serve
```

### Used apis
#### GET api/users
#### GET api/users/1
#### PUT api/users/1
#### POST api/users
#### DELETE api/users/1
#### GET api/users/1/payments
