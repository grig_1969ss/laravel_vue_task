import axios from "../plugins/axios";

export default {
    state: () => ({
        users:[],
        userInfo:[],
        userPayments:[],
    }),
    actions: {
        async login(context , data) {
          return  axios.post('/login', data ).then(res => {
                context.commit('setLogin', res.data)
              return res;
            })
        },
        async register(context , data) {
            axios.post("/register", data).then(res => {
                context.commit('setRegister', res.data)
            })
        },
        async getUsers(context) {
            axios.get('/users').then(res => {
                context.commit('setUsers', res.data)
            })
        },
        async userInfo(context , id) {
            axios.get(`/users/${id}`).then(res => {
                context.commit('setMenuInfo', res.data)
            })
        },
        async updateUser(context, userinfo) {
            let data = {
                name: userinfo.name,
                email: userinfo.email
            }
           await axios.put(`/users/${userinfo.id}`,data)
        },
       async deleteUser(context, userinfo) {
           await axios.delete(`/users/${userinfo.id}`, userinfo.user)
       },
       async createUser(context, data) {
           axios.post( '/users', data).then(res => {
               context.commit("setCreateUser", res.data)
            })
       } ,
       async paymentUser(context, userinfo) {
           axios.get( `/users/${userinfo.id}/payments`).then(res => {
               context.commit('setPaymentsUser' , res.data)
           })
       }
    },
    mutations: {
        setLogin(state,data) {
            localStorage.setItem("login" , JSON.stringify(data))
        } ,
        setRegister(state,data) {
            localStorage.setItem("register" , JSON.stringify(data))
        },
        setUsers(state,data) {
            state.users = data
        },
        setMenuInfo(state,data) {
            state.userInfo = data
        },
        setCreateUser(state,data) {
            state.users.push(data)
        },
        setPaymentsUser(state, data) {
            state.userPayments = data
        }
    },
    getters: {

    }

}