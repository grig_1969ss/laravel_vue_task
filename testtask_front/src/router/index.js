import Vue from 'vue'
import VueRouter from 'vue-router'
import UserLogin from '../components/UserLogin'
import UserRegister from '../components/UserRegister'
import Users from '../components/Users'
import CreateUser from '../components/CreateUser'
import User from '../components/User'

Vue.use(VueRouter)

const routes= [
   {
        path:'/login',
        name:"userLogin",
        component: UserLogin,
       meta: {
           auth: true
       }
    },
    {
        path:'/register',
        name:"register",
        component: UserRegister,
        meta: {
            auth: true
        }
    },
    {
        path:'/',
        name:"users",
        component: Users
    },
    {
        path:'/create',
        name:"create",
        component: CreateUser
    },
    {
        path:'/user/:id',
        name:"user",
        component: User
    },

]

const router = new VueRouter({
    routes,
    mode:'history'
})
router.beforeEach(
    (to, from, next) => {
        if(to.matched.some(record => record.meta.forVisitors)) {
            next()
        } else if(to.matched.some(record => record.meta.forAuth)) {
            if(!Vue.auth.isAuthenticated()) {
                next({
                    path: '/login'
                    // Redirect to original path if specified
                })
            } else {
                next()
            }
        } else {
            next()
        }
    }
)
export default router