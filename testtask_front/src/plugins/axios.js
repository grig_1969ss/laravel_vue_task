import axios from "axios";
axios.defaults.baseURL = 'http://127.0.0.1:8000/api';
const token  = localStorage.getItem('login') ? JSON.parse( localStorage.getItem('login')).token: '';
axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
export default axios