<?php
declare(strict_types=1);

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class Repository implements RepositoryInterface
{
    protected $model;
    protected $with = [];
    protected $orderBy = 'id';
    protected $orderDirection = 'asc';

    /**
     *  Construct.
     *
     * @param Model $model
     * @return void
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get all instances of model.
     *
     * @return Collection|null
     */
    public function allData(): ?Collection
    {
        return $this->model->orderBy('created_at', 'desc')->get();
    }

    /**
     *  Create a new record in the database.
     *
     * @param array $data
     * @return void
     */
    public function create(array $data): void
    {
        $this->model->create($data);
    }

    /**
     *  Find record in the database.
     *
     * @param int $id
     * @return Model
     */
    public function find(int $id): ?Model
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Update record in the database.
     *
     * @param array $data
     * @param $where
     * @return void
     */
    public function update(array $data, $where): void
    {
        $record = $this->model->findOrFail($where);

        $record->update($data);
    }

    /**
     * Remove record from the database
     *
     * @param $data
     * @return void
     */
    public function delete($data): void
    {
        $this->model->destroy($data);
    }

    /**
     * Order by record from the database
     *
     * @param string $orderBy
     * @param string $orderDirection
     * @return Repository
     */
    public function orderBy(string $orderBy, string $orderDirection): Repository
    {
        $this->orderBy = $orderBy;
        $this->orderDirection = $orderDirection;

        return $this;
    }

    /**
     *  Where record in the database.
     *
     * @param array $data
     * @return Builder[]|Collection
     */
    public function where(array $data): Collection
    {
        return $this->query()->where($data)->get();
    }

    /**
     * Sets relations for eager loading.
     *
     * @param $relations
     * @return Repository
     */
    public function with($relations): Repository
    {
        if (is_string($relations)) {
            $this->with = explode(',', $relations);

            return $this;
        }
        $this->with = is_array($relations) ? $relations : [];

        return $this;
    }

    /**
     * Creates a new QueryBuilder instance including eager loads
     *
     * @return Builder
     */
    protected function query(): Builder
    {
        return $this->model->newQuery()->with($this->with)->orderBy($this->orderBy, $this->orderDirection);
    }
}
